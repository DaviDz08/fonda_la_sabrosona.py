if __name__ == "__main__":

    menu = {
        "1. Mondongo" : 1.50,
        "2. Lengua" : 0.75,
        "3. Pata" : 2.00
    }

    print("FONDA LA SABROSONA")
    print("------------------\n")

    cliente = input("Cliente: ")

    while True:
        print("\nMENU")
        print(menu)
        try:
            opcion = int(input("Opcion: "))
        except:
            opcion = -1

        if opcion == 1:
            e = menu["1. Mondongo"]
            comida = "Mondongo"
            break
        elif opcion == 2:
            e = menu["2. Lengua"]
            comida = "Lengua"
            break
        elif opcion == 3:
            e = menu["3. Pata"]
            comida = "Pata"
            break
        else:
            print("Opcion invalida")

    total = e * 1.07

    print(cliente + ", debe pagar $" + str(total))
    pago = input("pago? (S/N)")
    if pago.upper() == "S":
        print("Toma tu",comida + ",", cliente)
    else:
        print("PEDIDO CANCELADO!!!!!!!")